#include <stdlib.h>

#include <brt/core/log.h>
#include <brt/renderer/renderer.h>

int main()
{

    if (!brt_renderer_initialize()) {
        brt_logf("Failed to initialize rendererer");
        exit(EXIT_FAILURE);
    }

    brt_renderer_shutdown();

    return 0;
}
