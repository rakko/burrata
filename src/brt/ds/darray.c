#include <stdlib.h>
#include <string.h>

#include <brt/ds/darray.h>

void *_darray_create(uint64_t capacity, uint64_t stride)
{
    uint64_t elements_size = capacity * stride;
    uint64_t *ptr = (uint64_t *) calloc(1, DARRAY_HEADER_SIZE + elements_size);
    if (!ptr)
        return NULL;
    ptr[DARRAY_FIELD_CAPACITY] = capacity;
    ptr[DARRAY_FIELD_LENGTH] = 0;
    ptr[DARRAY_FIELD_STRIDE] = stride;
    return (void *)(ptr + DARRAY_FIELD_COUNT);
}

void _darray_destroy(void *arr)
{
    uint64_t *header = (uint64_t *)arr - DARRAY_FIELD_COUNT;
    free(header);
}

uint64_t _darray_field_get(void *arr, uint64_t field)
{
    uint64_t *header = (uint64_t *)arr - DARRAY_FIELD_COUNT;
    return header[field];
}

void _darray_field_set(void *arr, uint64_t field, uint64_t value)
{
    uint64_t *header = (uint64_t *)arr - DARRAY_FIELD_COUNT;
    header[field] = value;
}

void *_darray_resize(void *arr)
{
    uint64_t capacity = darray_capacity(arr);
    uint64_t length = darray_length(arr);
    uint64_t stride = darray_stride(arr);
    void *temp = _darray_create(DARRAY_RESIZE_FACTOR * capacity, stride);
    memcpy(temp, arr, length * stride);
    _darray_field_set(temp, DARRAY_FIELD_LENGTH, length);
    _darray_destroy(arr);
    return temp;
}

void *_darray_push(void *arr, const void *pvalue)
{
    uint64_t capacity = darray_capacity(arr);
    uint64_t length = darray_length(arr);
    uint64_t stride = darray_stride(arr);
    if (length >= capacity) {
        arr = _darray_resize(arr);
    }

    uint8_t *place = (uint8_t *)arr + (length * stride);
    memcpy((void *)place, pvalue, stride);
    _darray_field_set(arr, DARRAY_FIELD_LENGTH, length + 1);
    return arr;
}

void _darray_pop(void *arr, void *dst)
{
    uint64_t length = darray_length(arr);
    uint64_t stride = darray_stride(arr);
    if (length == 0)
        return;

    uint8_t *place = (uint8_t *)arr + ((length - 1) * stride);
    memcpy(dst, place, stride);
    _darray_field_set(arr, DARRAY_FIELD_LENGTH, length - 1);
}
