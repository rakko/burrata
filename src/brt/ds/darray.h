#pragma once

#include <stdint.h>

// Memory layout: 
// uint64_t capacity: number of elements that can be held
// uint64_t length: number of elements currently contained
// uint64_t stride: size of each element in bytes
// void *elements

enum
{
    DARRAY_FIELD_CAPACITY,
    DARRAY_FIELD_LENGTH,
    DARRAY_FIELD_STRIDE,
    DARRAY_FIELD_COUNT
};

#define DARRAY_HEADER_SIZE (DARRAY_FIELD_COUNT * sizeof(uint64_t))

void *_darray_create(uint64_t capacity, uint64_t stride);
void _darray_destroy(void *arr);

uint64_t _darray_field_get(void *arr, uint64_t field);
void _darray_field_set(void *arr, uint64_t field, uint64_t value);

void *_darray_resize(void *arr);

void *_darray_push(void *arr, const void *pvalue);
void _darray_pop(void *arr, void *dst);

#define DARRAY_DEFAULT_CAPACITY 1
#define DARRAY_RESIZE_FACTOR 2

#define darray(type) type*

#define darray_create(type) \
    _darray_create(DARRAY_DEFAULT_CAPACITY, sizeof(type))

#define darray_reserve(type, capacity) \
    _darray_create(capacity, sizeof(type))

#define darray_destroy(arr) _darray_destroy(arr);

#define darray_push(arr, value)         \
    {                                   \
        typeof(value) temp = value;     \
        arr = _darray_push(arr, &temp); \
    }

#define darray_pop(arr, value_ptr) \
    _darray_pop(arr, value_ptr)

#define darray_clear(array) \
    _darray_field_set(array, DARRAY_FIELD_LENGTH, 0)

#define darray_capacity(array) \
    _darray_field_get(array, DARRAY_FIELD_CAPACITY)

#define darray_length(array) \
    _darray_field_get(array, DARRAY_FIELD_LENGTH)

#define darray_stride(array) \
    _darray_field_get(array, DARRAY_FIELD_STRIDE)

#define darray_length_set(array, value) \
    _darray_field_set(array, DARRAY_FIELD_LENGTH, value)
