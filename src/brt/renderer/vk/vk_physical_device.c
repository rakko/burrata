#include <brt/renderer/vk/vk_physical_device.h>

#include <brt/renderer/vk/vk_utils.h>

#include <brt/core/log.h>

static VkPhysicalDevice vk_physical_device_select_handle(VkPhysicalDevice *physical_devices,
                                                         uint32_t physical_devices_count)
{
    static uint32_t device_priority[VK_PHYSICAL_DEVICE_TYPE_CPU + 1] = {
        [VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU] = 10000,
        [VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU] = 1000,
        [VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU] = 100,
        [VK_PHYSICAL_DEVICE_TYPE_CPU] = 10,
        [VK_PHYSICAL_DEVICE_TYPE_OTHER] = 1,
    };

    uint32_t max_score = 0;
    ssize_t preferred_device_index = -1;
    for (uint32_t i = 0; i < physical_devices_count; ++i) {
        VkPhysicalDeviceProperties props;
        vkGetPhysicalDeviceProperties(physical_devices[i], &props);
        uint32_t score = device_priority[props.deviceType];
        if (score > max_score) {
            max_score = score;
            preferred_device_index = i;
        }
    }

    if (preferred_device_index == -1)
        return VK_NULL_HANDLE;

    return physical_devices[preferred_device_index];
}

static VkPhysicalDevice vk_physical_device_select_handle_with_type(VkPhysicalDevice *physical_devices,
                                                                   uint32_t physical_devices_count,
                                                                   VkPhysicalDeviceType type)
{
    for (int i = 0; i < physical_devices_count; ++i) {
        VkPhysicalDevice physical_device = physical_devices[i];
        VkPhysicalDeviceProperties props;
        vkGetPhysicalDeviceProperties(physical_device, &props);
        if (props.deviceType == type) {
            return physical_device;
        }
    }
    return VK_NULL_HANDLE;
}

static void vk_physical_device_init_properties(struct vk_physical_device *physical_device)
{
    vkGetPhysicalDeviceProperties(physical_device->handle, &physical_device->properties);
    vkGetPhysicalDeviceMemoryProperties(physical_device->handle, &physical_device->memory_properties);

    physical_device->queue_family_properties_count = BRT_VK_MAX_QUEUE_FAMILY_PROPERTIES;
    vkGetPhysicalDeviceQueueFamilyProperties(physical_device->handle,
                                             &physical_device->queue_family_properties_count,
                                             physical_device->queue_family_properties);
}

VkResult vk_physical_device_create(struct vk_context *vk_ctx, struct vk_physical_device *out_physical_device)
{
    uint32_t physical_devices_count = 0;
    VK_TRY(vkEnumeratePhysicalDevices(vk_ctx->instance, &physical_devices_count, NULL));
    VkPhysicalDevice physical_devices[physical_devices_count];
    VK_TRY(vkEnumeratePhysicalDevices(vk_ctx->instance, &physical_devices_count, physical_devices));

    VkPhysicalDevice physical_device = vk_physical_device_select_handle(physical_devices, physical_devices_count);
    if (physical_device == VK_NULL_HANDLE)
        return VK_ERROR_INITIALIZATION_FAILED;

    out_physical_device->handle = physical_device;
    vk_physical_device_init_properties(out_physical_device);

    return VK_SUCCESS;
}

VkResult vk_physical_device_create_with_type(struct vk_context *vk_ctx,
                                             VkPhysicalDeviceType type,
                                             struct vk_physical_device *out_physical_device)
{
    uint32_t physical_devices_count = 0;
    VK_TRY(vkEnumeratePhysicalDevices(vk_ctx->instance, &physical_devices_count, NULL));
    VkPhysicalDevice physical_devices[physical_devices_count];
    VK_TRY(vkEnumeratePhysicalDevices(vk_ctx->instance, &physical_devices_count, physical_devices));

    VkPhysicalDevice physical_device = vk_physical_device_select_handle_with_type(physical_devices, physical_devices_count, type);
    if (physical_device == VK_NULL_HANDLE)
        return VK_ERROR_INITIALIZATION_FAILED;

    out_physical_device->handle = physical_device;
    vk_physical_device_init_properties(out_physical_device);

    return VK_SUCCESS;
}

bool vk_physical_device_get_queue_index(struct vk_physical_device *physical_device,
                                        VkQueueFlagBits flag_bits,
                                        uint32_t *out_index)
{
    assert(physical_device);
    assert(out_index);

    uint32_t queue_index = UINT32_MAX;
    for (size_t i = 0; i < physical_device->queue_family_properties_count; i++) {
        if (physical_device->queue_family_properties[i].queueFlags & flag_bits) {
            queue_index = i;
            break;
        }
    }

    if (queue_index == UINT32_MAX)
        return false;

    *out_index = queue_index;

    return true;
}
