#pragma once

#include <vulkan/vulkan.h>

struct brt_window;

struct vk_renderpass
{
    VkRenderPass handle;
};

#define BRT_VK_SWAPCHAIN_IMAGE_COUNT 2

struct vk_swapchain
{
    VkSwapchainKHR handle;

    VkFormat format;
    VkExtent2D extent;

    uint32_t image_count;
    VkImage images[BRT_VK_SWAPCHAIN_IMAGE_COUNT];
    VkImageView image_views[BRT_VK_SWAPCHAIN_IMAGE_COUNT];
};

// As of Vulkan 1.3 we have 6 present modes (VkPresentModeKHR).
// This will need to be updated if more are added in the future
#define BRT_VK_MAX_PRESENT_MODES 6

struct vk_surface
{
    VkSurfaceKHR handle;

    VkSurfaceFormatKHR format;
    VkColorSpaceKHR colorspace;

    uint32_t  present_modes_count;
    VkPresentModeKHR present_modes[BRT_VK_MAX_PRESENT_MODES];

    VkSurfaceCapabilitiesKHR capabilities;

    struct brt_window *window;
};

// Abitrarily chosen for now. This can be adjusted if needed.
#define BRT_VK_MAX_QUEUE_FAMILY_PROPERTIES 10

struct vk_physical_device
{
    VkPhysicalDevice handle;

    VkPhysicalDeviceProperties properties;
    VkPhysicalDeviceMemoryProperties memory_properties;

    VkQueueFamilyProperties queue_family_properties[BRT_VK_MAX_QUEUE_FAMILY_PROPERTIES];
    uint32_t queue_family_properties_count;
};

struct vk_device
{
    VkDevice handle;

    VkQueue gfx_queue;
    VkQueue present_queue;
};

struct vk_context
{
    VkInstance instance;
    VkAllocationCallbacks *allocator;

#ifndef NDEBUG
    VkDebugUtilsMessengerEXT debug_utils_messenger;
#endif

    struct vk_physical_device physical_device;
    struct vk_surface surface;
    struct vk_device device;
    struct vk_swapchain swapchain;
    struct vk_renderpass renderpass;
};
