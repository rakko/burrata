#pragma once

#include <brt/renderer/vk/vk_types.h>

VkResult vk_device_create(struct vk_context *vk_ctx, struct vk_device *out_device);

void vk_device_destroy(struct vk_context *ctx, struct vk_device *device);
