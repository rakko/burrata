#include <brt/renderer/vk/vk_renderpass.h>

#include <brt/renderer/vk/vk_utils.h>

// TODO: need to make this way more configurable
VkResult vk_renderpass_create(struct vk_context *vk_ctx,
                              struct vk_renderpass *out_renderpass)
{
    struct vk_device *device = &vk_ctx->device;
    struct vk_swapchain *swapchain = &vk_ctx->swapchain;
    VkAllocationCallbacks *allocator = vk_ctx->allocator;

    VkAttachmentDescription color_attachment_desc = {
        .format = swapchain->format,
        /* No multisampling (i.e. only sampling once) */
        .samples = VK_SAMPLE_COUNT_1_BIT,
        /* What to do with data before rendering */
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        /* What to do with data after rendering */
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        /* Not using stencil buffer */
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        /* Not using stencil buffer */
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        /* Undefined since buffer is cleared before rendering */
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        /* Layout suitable for presenting to a VkSurfaceKHR */
        .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
    };

    VkAttachmentReference color_attachment_ref = {
        .attachment = 0,
        .layout     = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    };

    VkSubpassDescription subpass_desc = {
        .pipelineBindPoint    = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .colorAttachmentCount = 1,
        .pColorAttachments    = &color_attachment_ref,
    };

    /* Dependencies of the subpass described above
     *
     * srcSubpass == VK_SUBPASS_EXTERNAL  =>  commands that occur before vkCmdBeginRenderPass
     * dstSubpass == VK_SUBPASS_EXTERNAL => commands that occur after vkCmdEndRenderPass
     * dstSubpass must always be > srcSubpass unless one is VK_SUBPASS_EXTERNAL */
    VkSubpassDependency subpass_dep = {
        .srcSubpass    = VK_SUBPASS_EXTERNAL,
        .dstSubpass    = 0,
        .srcStageMask  = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        .dstStageMask  = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        .srcAccessMask = 0,
        .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
    };

    VkRenderPassCreateInfo render_pass_info = {
        .sType           = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .attachmentCount = 1,
        .pAttachments    = &color_attachment_desc,
        .subpassCount    = 1,
        .pSubpasses      = &subpass_desc,
        .dependencyCount = 1,
        .pDependencies   = &subpass_dep,
    };

    VkRenderPass handle;
    VK_TRY(vkCreateRenderPass(device->handle, &render_pass_info, allocator, &handle));

    out_renderpass->handle = handle;

    return VK_SUCCESS;
}

void vk_renderpass_destroy(struct vk_context *vk_ctx,
                           struct vk_renderpass *renderpass)
{
    vkDestroyRenderPass(vk_ctx->device.handle, renderpass->handle, vk_ctx->allocator);
}

