#pragma once

#include <brt/renderer/vk/vk_types.h>

VkResult vk_renderpass_create(struct vk_context *vk_ctx,
                              struct vk_renderpass *out_renderpass);

void vk_renderpass_destroy(struct vk_context *vk_ctx,
                           struct vk_renderpass *renderpass);
