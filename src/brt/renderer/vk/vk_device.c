#include <brt/renderer/vk/vk_device.h>

#include <brt/renderer/vk/vk_physical_device.h>
#include <brt/renderer/vk/vk_utils.h>

#include <stdint.h>

static const char *required_device_extensions[] = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME,
};

VkResult vk_device_create(struct vk_context *vk_ctx, struct vk_device *out_device)
{
    struct vk_physical_device *physical_device = &vk_ctx->physical_device;
    struct vk_surface *surface = &vk_ctx->surface;
    VkAllocationCallbacks *allocator = vk_ctx->allocator;

    uint32_t gfx_queue_family_index = 0;
    if (!vk_physical_device_get_queue_index(physical_device,
                                            VK_QUEUE_GRAPHICS_BIT,
                                            &gfx_queue_family_index))
        return VK_ERROR_INITIALIZATION_FAILED;

    // Check if graphics queue also supports presentation to a surface
    VkBool32 presentation_support = VK_FALSE;
    VK_TRY(vkGetPhysicalDeviceSurfaceSupportKHR(physical_device->handle,
                                                gfx_queue_family_index,
                                                surface->handle,
                                                &presentation_support));

    if (!presentation_support) {
        brt_loge("Graphics queue does not have surface presentation support" );
        return VK_ERROR_INITIALIZATION_FAILED;
    }

    // TODO: Support separate queue families for graphics and surface presentation
    float queue_priority = 1.0;
    VkDeviceQueueCreateInfo queue_info = {
        .sType            = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
        .queueFamilyIndex = gfx_queue_family_index,
        .queueCount       = 1,
        .pQueuePriorities = &queue_priority,
    };
    VkPhysicalDeviceFeatures device_features = {
        // Enable use of VK_POLYGON_MODE_POINT and VK_POLYGON_MODE_LINE
        .fillModeNonSolid = VK_TRUE
    };

    VkDeviceCreateInfo device_info = {
        .sType                   = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .queueCreateInfoCount    = 1,
        .pQueueCreateInfos       = &queue_info,
        .enabledExtensionCount   = ARRAY_SIZE(required_device_extensions),
        .ppEnabledExtensionNames = required_device_extensions,
        .pEnabledFeatures        = &device_features,
    };

    VkDevice device;
    VK_TRY(vkCreateDevice(physical_device->handle, &device_info, allocator, &device));

    // TODO: Remember to retrieve present queue when supporting separate queues for presentation and graphics
    VkQueue graphics_queue;
    vkGetDeviceQueue(device, gfx_queue_family_index, 0, &graphics_queue);

    out_device->handle = device;
    out_device->gfx_queue = graphics_queue;
    out_device->present_queue = graphics_queue;

    return VK_SUCCESS;
}

void vk_device_destroy(struct vk_context *vk_ctx, struct vk_device *device)
{
    vkDestroyDevice(device->handle, vk_ctx->allocator);
}
