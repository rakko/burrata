#pragma once

#include <brt/renderer/vk/vk_types.h>

#include <brt/renderer/renderer_defines.h>

VkResult vk_physical_device_create(struct vk_context *vk_ctx, struct vk_physical_device *out_physical_device);
VkResult vk_physical_device_create_with_type(struct vk_context *vk_ctx,
                                             VkPhysicalDeviceType type,
                                             struct vk_physical_device *out_physical_device);

bool vk_physical_device_get_queue_index(struct vk_physical_device *physical_device,
                                        VkQueueFlagBits flag_bits,
                                        uint32_t *out_index);
