#include <brt/renderer/vk/vk_swapchain.h>

#include <brt/platform/platform.h>
#include <brt/renderer/vk/vk_surface.h>
#include <brt/renderer/vk/vk_utils.h>

VkResult vk_swapchain_create(struct vk_context *vk_ctx,
                             struct vk_swapchain *out_swapchain)
{
    struct VkAllocationCallbacks *allocator = vk_ctx->allocator;
    struct vk_device *device = &vk_ctx->device;
    struct vk_surface *surface = &vk_ctx->surface;
    VkSurfaceCapabilitiesKHR *surface_caps = &surface->capabilities;
    /* FIFO is gauranteed by the spec to be supporrted */
    VkPresentModeKHR present_mode = VK_PRESENT_MODE_FIFO_KHR;
    VkSurfaceTransformFlagBitsKHR pre_transform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    VkSwapchainKHR old_swapchain = out_swapchain->handle;

    /* Prefer MAILBOX present mode if it is supported */
    if (vk_surface_has_present_mode(surface, VK_PRESENT_MODE_MAILBOX_KHR))
        present_mode = VK_PRESENT_MODE_MAILBOX_KHR;

    /* width and height are either both UINT32_MAX or both not UINT32_MAX.
     * If the surface size is undefined, the size is set to the size of the images requested.
     * Otherwise, the swapchain size must match. */
    VkExtent2D image_extent = surface_caps->currentExtent;
    if (surface_caps->currentExtent.width == UINT32_MAX) {
        uint32_t window_w = surface->window->width; 
        uint32_t window_h = surface->window->height; 
        uint32_t min_w = surface_caps->minImageExtent.width;
        uint32_t min_h = surface_caps->minImageExtent.height;
        uint32_t max_w = surface_caps->maxImageExtent.width;
        uint32_t max_h = surface_caps->maxImageExtent.height;
        image_extent.width =
            window_w < min_w ? min_w :
                window_w > max_w ? max_w : window_w;
        image_extent.height =
            window_h < min_h ? min_h :
                window_h > max_h ? max_h : window_h;
    }

    uint32_t image_count = BRT_VK_SWAPCHAIN_IMAGE_COUNT;
    if (image_count < surface_caps->minImageCount)
        image_count = surface_caps->minImageCount;
    if (surface_caps->maxImageCount > 0 && image_count > surface_caps->maxImageCount)
        image_count = surface_caps->maxImageCount;

    if (!(surface_caps->supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR))
        pre_transform = surface_caps->currentTransform;

    VkSwapchainCreateInfoKHR create_info = {
        .sType                 = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .surface               = surface->handle,
        .minImageCount         = image_count,
        .imageFormat           = surface->format.format,
        .imageColorSpace       = surface->colorspace,
        .imageExtent           = image_extent,
        .imageArrayLayers      = 1, // Always 1 unless doing stereoscopic 3D
        .imageUsage            = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        .imageSharingMode      = VK_SHARING_MODE_EXCLUSIVE, // We are using only one queue family
        .preTransform          = pre_transform,
        .compositeAlpha        = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        .presentMode           = present_mode,
        .clipped               = VK_TRUE,
        .oldSwapchain          = old_swapchain,
    };

    // TODO: When we support separate queues for graphics and presentation
    // Had our graphics queue family and present queue family been different then we would have opted for:
    //
    // .imageSharingMode      = VK_SHARING_MODE_CONCURRENT,
    // .queueFamilyIndexCount = 2,
    // .pQueueFamilyIndices   = queue_family_indices,

    VkSwapchainKHR swapchain;
    VK_TRY(vkCreateSwapchainKHR(device->handle, &create_info, allocator, &swapchain));

    if (old_swapchain != VK_NULL_HANDLE)
        vkDestroySwapchainKHR(device->handle, old_swapchain, allocator);

    VkResult res = vkGetSwapchainImagesKHR(device->handle,
                                           swapchain,
                                           &image_count,
                                           out_swapchain->images);
    if (res != VK_SUCCESS && res != VK_INCOMPLETE) {
        vkDestroySwapchainKHR(device->handle, swapchain, allocator);
        return res;
    }

    for (uint32_t i = 0; i < image_count; ++i) {
        VkImageViewCreateInfo image_view_info = {
            .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            .image = out_swapchain->images[i],
            .viewType = VK_IMAGE_VIEW_TYPE_2D,
            .format = surface->format.format,
            .components = {
                .r = VK_COMPONENT_SWIZZLE_IDENTITY,
                .g = VK_COMPONENT_SWIZZLE_IDENTITY,
                .b = VK_COMPONENT_SWIZZLE_IDENTITY,
                .a = VK_COMPONENT_SWIZZLE_IDENTITY,
            },
            .subresourceRange = {
                .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .baseMipLevel = 0,
                .levelCount = 1,
                .baseArrayLayer = 0,
                .layerCount = 1,
            },
        };
        vkCreateImageView(device->handle, &image_view_info, allocator, &out_swapchain->image_views[i]);
    }

    out_swapchain->image_count = image_count;
    out_swapchain->handle = swapchain;
    out_swapchain->format = surface->format.format;
    out_swapchain->extent = image_extent;

    return VK_SUCCESS;
}

void vk_swapchain_destroy(struct vk_context *vk_ctx, struct vk_swapchain *swapchain)
{
    for (uint32_t i = 0; i < swapchain->image_count; ++i)
        vkDestroyImageView(vk_ctx->device.handle, swapchain->image_views[i], vk_ctx->allocator);
    vkDestroySwapchainKHR(vk_ctx->device.handle, swapchain->handle, vk_ctx->allocator);
}
