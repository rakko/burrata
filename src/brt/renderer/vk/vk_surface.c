#include <brt/renderer/vk/vk_surface.h>

#include <brt/platform/platform.h>
#include <brt/renderer/vk/vk_utils.h>

#include <string.h>

VkResult vk_surface_create(struct vk_context *vk_ctx, struct vk_surface *out_surface)
{
    struct vk_physical_device *physical_device = &vk_ctx->physical_device;

    struct brt_window *window = brt_platform_window_create("test", 0, 0, 800, 600);
    if (!window)
        return VK_ERROR_INITIALIZATION_FAILED;

    VkSurfaceKHR surface = brt_platform_vk_surface_create(vk_ctx);
    if (surface == VK_NULL_HANDLE)
        return VK_ERROR_INITIALIZATION_FAILED;

    VkSurfaceCapabilitiesKHR capabilities;
    VK_TRY(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device->handle, surface, &capabilities));

    /* We only really care about the first one.
     * If the format list includes just one entry of VK_FORMAT_UNDEFINED,
     * the surface has no preferred format. Otherwise, at least one
     * supported format will be returned so we just stick with the
     * first.
     */
    uint32_t formats_count = 1;
    VkSurfaceFormatKHR surface_format;
    VkResult res =
        vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device->handle,
                                             surface,
                                             &formats_count,
                                             &surface_format);
    if (res != VK_SUCCESS && res !=VK_INCOMPLETE)
        return res;

    VkColorSpaceKHR colorspace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
    if (surface_format.format == VK_FORMAT_UNDEFINED)
        surface_format.format = VK_FORMAT_B8G8R8A8_UNORM;
    else
        colorspace = surface_format.colorSpace;

    out_surface->present_modes_count = BRT_VK_MAX_PRESENT_MODES;
    VK_TRY(vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device->handle,
                                                     surface,
                                                     &out_surface->present_modes_count,
                                                     out_surface->present_modes));

    out_surface->handle = surface;
    out_surface->format = surface_format;
    out_surface->colorspace = colorspace;
    out_surface->capabilities = capabilities;
    out_surface->window = window;

    return VK_SUCCESS;
}

void vk_surface_destroy(struct vk_context *vk_ctx, struct vk_surface *surface)
{
    vkDestroySurfaceKHR(vk_ctx->instance, surface->handle, vk_ctx->allocator);
    brt_platform_window_destroy(surface->window);
    memset(surface, 0, sizeof(*surface));
}

bool vk_surface_has_present_mode(struct vk_surface *surface, VkPresentModeKHR present_mode)
{
    for (uint32_t i = 0; i < surface->present_modes_count; ++i) {
        if (surface->present_modes[i] == present_mode)
            return true;
    }
    return false;
}
