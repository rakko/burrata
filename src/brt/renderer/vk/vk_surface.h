#pragma once

#include <brt/renderer/vk/vk_types.h>

#include <brt/defines.h>

VkResult vk_surface_create(struct vk_context *vk_ctx, struct vk_surface *out_surface);

void vk_surface_destroy(struct vk_context *vk_ctx, struct vk_surface *surface);

bool vk_surface_has_present_mode(struct vk_surface *surface, VkPresentModeKHR present_mode);

VkSurfaceKHR brt_platform_vk_surface_create(struct vk_context *vk_ctx);
