#pragma once

#include <brt/renderer/vk/vk_types.h>

#include <brt/core/log.h>

#define CASE(r) case r: return #r
static const char *vk_util_result_as_string(VkResult res)
{
    switch (res) {
    CASE(VK_SUCCESS);
    CASE(VK_NOT_READY);
    CASE(VK_TIMEOUT);
    CASE(VK_EVENT_SET);
    CASE(VK_EVENT_RESET);
    CASE(VK_INCOMPLETE);
    CASE(VK_ERROR_OUT_OF_HOST_MEMORY);
    CASE(VK_ERROR_OUT_OF_DEVICE_MEMORY);
    CASE(VK_ERROR_INITIALIZATION_FAILED);
    CASE(VK_ERROR_DEVICE_LOST);
    CASE(VK_ERROR_MEMORY_MAP_FAILED);
    CASE(VK_ERROR_LAYER_NOT_PRESENT);
    CASE(VK_ERROR_EXTENSION_NOT_PRESENT);
    CASE(VK_ERROR_FEATURE_NOT_PRESENT);
    CASE(VK_ERROR_INCOMPATIBLE_DRIVER);
    CASE(VK_ERROR_TOO_MANY_OBJECTS);
    CASE(VK_ERROR_FORMAT_NOT_SUPPORTED);
    CASE(VK_ERROR_FRAGMENTED_POOL);
    CASE(VK_ERROR_UNKNOWN);
    // Provided by VK_VERSION_1_1
    CASE(VK_ERROR_OUT_OF_POOL_MEMORY);
    CASE(VK_ERROR_INVALID_EXTERNAL_HANDLE);
    // Provided by VK_VERSION_1_2
    CASE(VK_ERROR_FRAGMENTATION);
    CASE(VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS);
    // Provided by VK_KHR_surface
    CASE(VK_ERROR_SURFACE_LOST_KHR);
    CASE(VK_ERROR_NATIVE_WINDOW_IN_USE_KHR);
    // Provided by VK_KHR_swapchain
    CASE(VK_SUBOPTIMAL_KHR);
    CASE(VK_ERROR_OUT_OF_DATE_KHR);
    // Provided by VK_KHR_display_swapchain
    CASE(VK_ERROR_INCOMPATIBLE_DISPLAY_KHR);
    // Provided by VK_EXT_debug_report
    CASE(VK_ERROR_VALIDATION_FAILED_EXT);
    default:
        return "UNKNOWN_ERROR";
    }
}
#undef CASE


#define VK_TRY(exp)                \
    {                              \
        VkResult res__ = (exp);    \
        if (res__ != VK_SUCCESS) { \
            brt_loge("'%s' failed. (%s)", #exp, vk_util_result_as_string(res__)); \
            return res__;          \
        }                          \
    }

#define VK_TRY_BOOL(exp)           \
    {                              \
        VkResult res__ = (exp);    \
        if (res__ != VK_SUCCESS) { \
            brt_loge("'%s' failed. (%s)", #exp, vk_util_result_as_string(res__)); \
            return false;          \
        }                          \
    }
