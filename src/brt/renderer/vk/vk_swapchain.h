#pragma once

#include <brt/renderer/vk/vk_types.h>

VkResult vk_swapchain_create(struct vk_context *vk_ctx,
                             struct vk_swapchain *out_swapchain);

void vk_swapchain_destroy(struct vk_context *vk_ctx, struct vk_swapchain *swapchain);
