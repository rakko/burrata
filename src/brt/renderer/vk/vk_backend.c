#include <brt/renderer/vk/vk_backend.h>

#include <brt/renderer/vk/vk_utils.h>
#include <brt/renderer/vk/vk_types.h>
#include <brt/renderer/vk/vk_physical_device.h>
#include <brt/renderer/vk/vk_device.h>
#include <brt/renderer/vk/vk_surface.h>
#include <brt/renderer/vk/vk_swapchain.h>
#include <brt/renderer/vk/vk_renderpass.h>

#include <brt/core/log.h>

#include <string.h>

static struct vk_context vk_ctx;

static const char *required_instance_layers[] = {
#ifndef NDEBUG
    // Khronos validation layer
    "VK_LAYER_KHRONOS_validation",
#endif
};
static const char *optional_instance_layers[] = {
    // Display info about the running application using an overlay
    "VK_LAYER_MESA_overlay",
};

static const char *required_instance_extensions[] = {
    VK_KHR_SURFACE_EXTENSION_NAME,
#ifndef NDEBUG
    VK_EXT_DEBUG_UTILS_EXTENSION_NAME,
#endif
#ifdef VK_USE_PLATFORM_XCB_KHR 
    VK_KHR_XCB_SURFACE_EXTENSION_NAME,
#endif
};

//static const char *required_device_extensions[] = {
//    VK_KHR_SWAPCHAIN_EXTENSION_NAME,
//};

#ifndef NDEBUG
VKAPI_ATTR VkBool32 VKAPI_CALL vk_debug_util_cb(
    VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
    VkDebugUtilsMessageTypeFlagsEXT message_types,
    const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
    void* user_data
);

VkResult vk_create_debug_utils_messenger();
void vk_destroy_debug_utils_messenger();
#endif //NDEBUG

static bool vk_check_required_instance_extensions(VkExtensionProperties *extensions, uint32_t extensions_count)
{
    uint32_t required_count = ARRAY_SIZE(required_instance_extensions);
    for (uint32_t r = 0; r < required_count; ++r) {
        bool supported = false;
        const char *required_ext_name = required_instance_extensions[r];
        for (uint32_t e = 0; e < extensions_count; ++e) {
            if (strcmp(extensions[e].extensionName, required_ext_name) == 0) {
                supported = true;
                break;
            }
        }

        if (!supported) {
            brt_loge("Missing required extension '%s'", required_ext_name);
            return false;
        }
    }

    return true;
}

static bool vk_check_required_instance_layers(VkLayerProperties *layers, uint32_t layers_count)
{
    uint32_t required_count = ARRAY_SIZE(required_instance_layers);
    for (uint32_t r = 0; r < required_count; ++r) {
        bool supported = false;
        const char *required_layer_name = required_instance_layers[r];
        for (uint32_t e = 0; e < layers_count; ++e) {
            if (strcmp(layers[e].layerName, required_layer_name) == 0) {
                supported = true;
                break;
            }
        }

        if (!supported) {
            brt_loge("Missing required layer '%s'", required_layer_name);
            return false;
        }
    }

    return true;
}

static uint32_t vk_check_optional_instance_layers(VkLayerProperties *layers, uint32_t layers_count)
{
    uint32_t optional_count = ARRAY_SIZE(optional_instance_layers);
    uint32_t swap_index = optional_count - 1;
    uint32_t supported_count = 0;

    for (uint32_t o = 0; o < optional_count; ++o) {
        bool supported = false;
        const char *optional_layer_name = optional_instance_layers[o];
        for (uint32_t l = 0; l < layers_count; ++l) {
            if (strcmp(layers[l].layerName, optional_layer_name) == 0) {
                supported = true;
                break;
            }
        }

        if (supported) {
            supported_count++;
        } else {
            brt_logi("Missing optional layer '%s'", optional_layer_name);
            if (o < swap_index) {
                // swap o with something we havent processed yet
                const char *temp = optional_layer_name;
                optional_instance_layers[o] = optional_instance_layers[swap_index];
                optional_instance_layers[swap_index] = temp;
                o--;
                swap_index--;
                continue;
            }
            // o >= swap_index means we're done
            break;
        }
    }

    return supported_count;
}

bool vk_initialize()
{
    brt_logi("Initializing Vulkan backend");

    // Layers
    uint32_t layers_count;
    VK_TRY_BOOL(vkEnumerateInstanceLayerProperties(&layers_count, 0));
    VkLayerProperties layers[layers_count];
    VK_TRY_BOOL(vkEnumerateInstanceLayerProperties(&layers_count, layers));

    //brt_logv("Available instance layers: ");
    //brt_logv("-------------------------------");
    //for (size_t i = 0; i < layers_count; ++i) {
    //    brt_logv("\t\t%s", layers[i].layerName);
    //}

    if(!vk_check_required_instance_layers(layers, layers_count))
        return false;

    const uint32_t required_layers_count = ARRAY_SIZE(required_instance_layers);
    const uint32_t optional_layers_count = vk_check_optional_instance_layers(layers, layers_count);
    const uint32_t enabled_layers_count = required_layers_count + optional_layers_count;

    const char *enabled_layers[enabled_layers_count];
    for (uint32_t i = 0; i < required_layers_count; ++i) {
        enabled_layers[i] = required_instance_layers[i];
    }
    for (uint32_t i = required_layers_count; i < enabled_layers_count; ++i) {
        enabled_layers[i] = optional_instance_layers[i - required_layers_count];
    }

    // Extensions
    uint32_t extensions_count;
    VK_TRY_BOOL(vkEnumerateInstanceExtensionProperties(0, &extensions_count, 0));
    VkExtensionProperties extensions[extensions_count];
    VK_TRY_BOOL(vkEnumerateInstanceExtensionProperties(0, &extensions_count, extensions));

    //brt_logv("Available instance extensions: ");
    //brt_logv("-------------------------------");
    //for (size_t i = 0; i < extensions_count; ++i) {
    //    brt_logv("\t\t%s", extensions[i].extensionName);
    //}

    if(!vk_check_required_instance_extensions(extensions, extensions_count))
        return false;
    const uint32_t required_extensions_count = ARRAY_SIZE(required_instance_extensions);
    const uint32_t enabled_extensions_count = required_extensions_count;

    const char *enabled_extensions[enabled_extensions_count];
    for (uint32_t i = 0; i < enabled_extensions_count; ++i) {
        enabled_extensions[i] = required_instance_extensions[i];
    }

    VkApplicationInfo app_info = {
        .sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pApplicationName   = "test",
        .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
        .pEngineName        = "BURRATA ENGINE",
        .engineVersion      = VK_MAKE_VERSION(1, 0, 0),
        .apiVersion         = VK_API_VERSION_1_2,
    };

    VkInstanceCreateInfo instance_info = {
        .sType                   = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pApplicationInfo        = &app_info,
        .enabledLayerCount       = enabled_layers_count,
        .ppEnabledLayerNames     = enabled_layers,
        .enabledExtensionCount   = enabled_extensions_count,
        .ppEnabledExtensionNames = enabled_extensions,
    };

    VK_TRY(vkCreateInstance(&instance_info, vk_ctx.allocator, &vk_ctx.instance));

    brt_logi("Vulkan instance created");

#ifndef NDEBUG
    VK_TRY(vk_create_debug_utils_messenger());
#endif

    VK_TRY(vk_physical_device_create(&vk_ctx, &vk_ctx.physical_device));

    brt_logi("Selected device '%s'", vk_ctx.physical_device.properties.deviceName);

    VK_TRY(vk_surface_create(&vk_ctx, &vk_ctx.surface));

    VK_TRY(vk_device_create(&vk_ctx, &vk_ctx.device));

    VK_TRY(vk_swapchain_create(&vk_ctx, &vk_ctx.swapchain));

    VK_TRY(vk_renderpass_create(&vk_ctx, &vk_ctx.renderpass));

    return true;
}

void vk_shutdown()
{
    brt_logi("Shutting down Vulkan backend");

    vk_renderpass_destroy(&vk_ctx, &vk_ctx.renderpass);

    vk_swapchain_destroy(&vk_ctx, &vk_ctx.swapchain);

    vk_device_destroy(&vk_ctx, &vk_ctx.device);

    vk_surface_destroy(&vk_ctx, &vk_ctx.surface);

#ifndef NDEBUG
    vk_destroy_debug_utils_messenger();
#endif

    vkDestroyInstance(vk_ctx.instance, vk_ctx.allocator);

    memset(&vk_ctx, 0, sizeof(vk_ctx));
}

#ifndef NDEBUG

VKAPI_ATTR VkBool32 VKAPI_CALL vk_debug_util_cb(
    VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
    VkDebugUtilsMessageTypeFlagsEXT message_types,
    const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
    void* user_data
) {
    switch (message_severity) {
        default:
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
            brt_loge("(debug_util) %s", callback_data->pMessage);
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
            brt_logw("(debug_util) %s", callback_data->pMessage);
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
            brt_logi("(debug_util) %s", callback_data->pMessage);
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
            brt_logv("(debug_util) %s", callback_data->pMessage);
            break;
    }

    // Should always return VK_FALSE
    return VK_FALSE;
}

VkResult vk_create_debug_utils_messenger()
{
    VkDebugUtilsMessengerCreateInfoEXT debug_utils_messenger_info = {
        .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
        .messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
            | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT
#if 0 // Enable if really needed
            | VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT
#endif
            | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT,
        .messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
            | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
            | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
        .pfnUserCallback = vk_debug_util_cb,
    };

    PFN_vkCreateDebugUtilsMessengerEXT create_debug_utils_messenger_ext_func =
        (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(vk_ctx.instance, "vkCreateDebugUtilsMessengerEXT");

    assert(create_debug_utils_messenger_ext_func);

    VK_TRY(create_debug_utils_messenger_ext_func(
        vk_ctx.instance,
        &debug_utils_messenger_info,
        vk_ctx.allocator,
        &vk_ctx.debug_utils_messenger
    ));

    return VK_SUCCESS;
}

void vk_destroy_debug_utils_messenger()
{
    PFN_vkDestroyDebugUtilsMessengerEXT destroy_debug_util_messenger_ext_func =
        (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(vk_ctx.instance, "vkDestroyDebugUtilsMessengerEXT");
    assert(destroy_debug_util_messenger_ext_func);
    destroy_debug_util_messenger_ext_func(
        vk_ctx.instance, vk_ctx.debug_utils_messenger, vk_ctx.allocator);
}
#endif // NDEBUG
