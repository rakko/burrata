#pragma once

#include <brt/renderer/renderer_defines.h>

enum brt_renderer_backend_type
{
    RENDERER_BACKEND_TYPE_VK,
    RENDERER_BACKEND_TYPE_COUNT
};

struct brt_renderer_backend
{
    enum brt_renderer_backend_type type;

    bool (*initialize)();
    void (*shutdown)();
};

bool brt_renderer_backend_create(enum brt_renderer_backend_type type, struct brt_renderer_backend *out_backend);
void brt_renderer_backend_destroy(struct brt_renderer_backend *backend);
