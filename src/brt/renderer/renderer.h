#pragma once

#include <brt/renderer/renderer_defines.h>

bool brt_renderer_initialize();
void brt_renderer_shutdown();
