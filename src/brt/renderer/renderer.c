#include <brt/renderer/renderer.h>
#include <brt/renderer/renderer_backend.h>

static struct brt_renderer
{
    struct brt_renderer_backend backend;
} renderer;


bool brt_renderer_initialize()
{
    if (!brt_renderer_backend_create(RENDERER_BACKEND_TYPE_VK, &renderer.backend))
        return false;

    return renderer.backend.initialize();
}

void brt_renderer_shutdown()
{
    renderer.backend.shutdown();
    brt_renderer_backend_destroy(&renderer.backend);
}
