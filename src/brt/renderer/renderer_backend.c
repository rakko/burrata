#include <brt/renderer/renderer_backend.h>
#include <brt/renderer/vk/vk_backend.h>

bool brt_renderer_backend_create(enum brt_renderer_backend_type type, struct brt_renderer_backend *out_backend)
{
    struct brt_renderer_backend backend = {};
    
    switch(type) {
        case RENDERER_BACKEND_TYPE_VK:
            backend.initialize = vk_initialize;
            backend.shutdown = vk_shutdown;
            break;
        default:
            return false;
    }

    backend.type = type;

    *out_backend = backend;

    return true;
}

void brt_renderer_backend_destroy(struct brt_renderer_backend *backend)
{
    backend->initialize = 0;
    backend->shutdown = 0;
}
