#pragma once

typedef enum log_level
{
    LOG_FATAL,
    LOG_ERROR,
    LOG_WARNING,
    LOG_INFO,
    LOG_DEBUG,
    LOG_VERBOSE,
} log_level;

void brt_log(enum log_level level, const char* file, int line, const char *fmt, ...);

#define brt_logf(...) brt_log(LOG_FATAL, __FILE__, __LINE__, __VA_ARGS__)
#define brt_loge(...) brt_log(LOG_ERROR, __FILE__, __LINE__, __VA_ARGS__)
#define brt_logw(...) brt_log(LOG_WARNING, __FILE__, __LINE__, __VA_ARGS__)
#define brt_logi(...) brt_log(LOG_INFO, __FILE__, __LINE__, __VA_ARGS__)
#ifndef NDEBUG
#define brt_logv(...) brt_log(LOG_VERBOSE, __FILE__, __LINE__, __VA_ARGS__)
#define brt_logd(...) brt_log(LOG_DEBUG, __FILE__, __LINE__, __VA_ARGS__)
#else
#define brt_logv(...)
#define brt_logd(...)
#endif
