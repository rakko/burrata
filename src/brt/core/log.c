#include <stdarg.h>
#include <stdio.h>

#include <brt/core/log.h>

// Log messages can be max 32768 bytes long
#define LOG_MSG_SIZE_MAX (1 << 15)

void brt_log(enum log_level level, const char *file, int line, const char *fmt, ...)
{
    static char buf[LOG_MSG_SIZE_MAX];
    static const char *level_abreviations[] = {
        "FTL", "ERR", "WRN", "INF", "DBG", "VRB"
    };
    static const char *level_colors[] = {
        "\033[1;31m", // bright red
        "\033[1;33m", // bright yellow
        "\033[35m", // magenta
        "\033[32m", // green
        "\033[34m", // blue
        "\033[37m"  // white
    };
    static const char *COLOR_RESET = "\033[0m";

    snprintf(buf, LOG_MSG_SIZE_MAX, "%s[%s](%s:%d) %s%s\n", level_colors[level], level_abreviations[level], file, line, fmt, COLOR_RESET);

    va_list va;
    va_start(va, fmt);

    // TODO: option to output to file
    if (level < (int)LOG_INFO) {
        vfprintf(stderr, buf, va);
    } else {
        vfprintf(stdout, buf, va);
    }

    va_end(va);
}
