#pragma once

#include <stdint.h>
#include <stddef.h>

#include <brt/defines.h>

struct brt_window
{
    const char *title;
    uint32_t width;
    uint32_t height;
};

struct brt_window *brt_platform_window_create(const char *window_title,
                                              uint32_t x,
                                              uint32_t y,
                                              uint32_t width,
                                              uint32_t height);

void brt_platform_window_destroy(struct brt_window *window);

void *brt_platform_allocate(size_t size);

void brt_platform_free(void *ptr);
