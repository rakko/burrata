#include <brt/platform/platform.h>

#include <brt/core/log.h>
#include <brt/renderer/vk/vk_types.h>

#include <stdlib.h>
#include <string.h>

#include <xcb/xcb.h>

static const char *WM_PROTOCOLS_STR = "WM_PROTOCOLS";
static const char *WM_DELETE_WINDOW_STR = "WM_DELETE_WINDOW";

struct brt_window_xcb
{
    struct brt_window base;
    xcb_window_t window;
    xcb_connection_t *connection;
    xcb_atom_t wm_delete_window;
};

static struct brt_window_xcb xcb_window;

static bool window_initialized;

struct brt_window *brt_platform_window_create(const char *window_title,
                                              uint32_t x,
                                              uint32_t y,
                                              uint32_t width,
                                              uint32_t height)
{
    brt_logi("Creating window '%s' (%dx%d)", window_title, width, height);

    // NULL means to connect to the display specified by the DISPLAY variable
    int scrn;
    xcb_connection_t *connection = xcb_connect(NULL, &scrn);
    if (!connection || xcb_connection_has_error(connection)) {
        return NULL;
    }

    xcb_window_t window = xcb_generate_id(connection);

    // Get the screen setup and loop through all the screens.
    const xcb_setup_t *setup = xcb_get_setup(connection);
    xcb_screen_iterator_t iter = xcb_setup_roots_iterator(setup);
    while (scrn-- > 0) {
        xcb_screen_next(&iter);
    }

    xcb_screen_t *screen = iter.data;

    // value_mask: Mask of the type of values we want to create our window with
    //   * XCB_CW_BACK_PIXEL: A pixmap of undefined size filled with the specified background
    //                        pixel is used for the background. Range checking is not performed,
    //                        the background pixel is truncated to the appropriate number of bits.
    //   * XCB_CW_EVENT_MASK: The event-mask defines which events the client is interested in for this window 
    //
    // value_list: The values for each of the aforementioned types specified in 'value_mask' in
    //             ascending order of the bit values of the value masks
    uint32_t value_mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
    uint32_t value_list[] = {
        screen->black_pixel,
        XCB_EVENT_MASK_EXPOSURE
        | XCB_EVENT_MASK_KEY_PRESS
        | XCB_EVENT_MASK_KEY_RELEASE
        | XCB_EVENT_MASK_BUTTON_PRESS
        | XCB_EVENT_MASK_BUTTON_RELEASE
        | XCB_EVENT_MASK_STRUCTURE_NOTIFY
    };

    xcb_create_window(connection,                    // connection
                      XCB_COPY_FROM_PARENT,          // window depth (inherit form parent)
                      window,                        // window ID
                      screen->root,                  // the parent window
                      x, y,                          // (x,y) pixel position of window top-left corner
                      width, height,                 // width and height of the window in pixels
                      0,                             // window border width in pixels
                      XCB_WINDOW_CLASS_INPUT_OUTPUT, // window class
                      screen->root_visual,           // window visual (inherit from parent)
                      value_mask, value_list);       // values

    // Change window name
    xcb_change_property(connection,            // connection
                        XCB_PROP_MODE_REPLACE, // mode of action on property (*_REPLACE, *_PREPEND, *_APPEND)
                        window,                // window whose property we want to change
                        XCB_ATOM_WM_NAME,      // property we want to change
                        XCB_ATOM_STRING,       // type of property we want to change
                        8,                     // bit representation of the data (8, 16, 32)
                        strlen(window_title),  // number of elements
                        window_title);         // property data

    // Request "WM_PROTOCOLS" property atom only if it exists
    xcb_intern_atom_cookie_t cookie_wm_protocols =
        xcb_intern_atom(connection, true, strlen(WM_PROTOCOLS_STR), WM_PROTOCOLS_STR);
    xcb_intern_atom_reply_t *wm_protocols =
        xcb_intern_atom_reply(connection, cookie_wm_protocols, NULL);

    // Request "WM_DELETE_WINDOW" property atom
    // Hold onto it so that we can check for this event when handling events in the main loop
    xcb_intern_atom_cookie_t cookie_wm_delete_window =
        xcb_intern_atom(connection, false, strlen(WM_DELETE_WINDOW_STR), WM_DELETE_WINDOW_STR);
    xcb_intern_atom_reply_t *wm_delete_window =
        xcb_intern_atom_reply(connection, cookie_wm_delete_window, NULL);
    xcb_atom_t wm_delete_window_atom = wm_delete_window->atom;

    // Request that we be notified when the window is to be deleted
    xcb_change_property(connection,              // connection
                        XCB_PROP_MODE_REPLACE,   // mode of action on property (*_REPLACE, *_PREPEND, *_APPEND)
                        window,                  // window whose property we want to change
                        wm_protocols->atom,      // property we want to change (an atom)
                        XCB_ATOM_ATOM,           // type of property we want to change (an atom)
                        32,                      // bit representation of the data (8, 16, 32 )
                        1,                       // number of elements
                        &wm_delete_window_atom); // property data

    brt_platform_free(wm_delete_window);
    brt_platform_free(wm_protocols);

    // Display our window
    xcb_map_window(connection, window);

    // Make sure all our requests have been sent
    xcb_flush(connection);

    xcb_window.base.title = window_title;
    xcb_window.base.width = width;
    xcb_window.base.height = height;
    xcb_window.window = window;
    xcb_window.connection = connection;
    xcb_window.wm_delete_window = wm_delete_window_atom;

    window_initialized = true;

    return (struct brt_window *)&xcb_window;
}

void brt_platform_window_destroy(struct brt_window *window)
{
    if (!window_initialized)
        return;

    assert(&xcb_window == window);

    brt_logi("Destroying window");

    xcb_destroy_window(xcb_window.connection, xcb_window.window);
    xcb_disconnect(xcb_window.connection);
    memset(&xcb_window, 0, sizeof(xcb_window));

    window_initialized = false;
}

void *brt_platform_allocate(size_t size)
{
    return malloc(size);
}

void brt_platform_free(void *ptr)
{
    free(ptr);
}

VkSurfaceKHR brt_platform_vk_surface_create(struct vk_context *vk_ctx)
{
    VkSurfaceKHR surface = VK_NULL_HANDLE;

    if (!window_initialized) {
        brt_logw("%s called with uninitialized window!", __func__);
        return surface;
    }

    VkXcbSurfaceCreateInfoKHR create_info = {
        .sType      = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR,
        .connection = xcb_window.connection,
        .window     = xcb_window.window,
    };

    if (vkCreateXcbSurfaceKHR(vk_ctx->instance, &create_info, vk_ctx->allocator, &surface) != VK_SUCCESS) {
        brt_logw("vkCreateXcbSurfaceKHR failed!");
        return VK_NULL_HANDLE;
    }

    return surface;
}
