#pragma once

#include <assert.h>

typedef _Bool bool;

#define  true 1
#define  false 0

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof(arr[0]))

#if defined(__clang__) || defined(__gcc__)
/** @brief Static assertion */
#define STATIC_ASSERT _Static_assert
#else
#define STATIC_ASSERT static_assert
#endif

// Platform detection
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
  #define BRT_PLATFORM_WINDOWS 1
  #ifndef _WIN64
    #error "64-bit is required on Windows!"
  #endif
#elif defined(__linux__) || defined(__gnu_linux__)
  // Linux OS
  #define BRT_PLATFORM_LINUX 1
  #if defined(__ANDROID__)
    #define BRT_PLATFORM_ANDROID 1
  #endif
#elif defined(__unix__)
  // Catch anything not caught by the above.
  #define BRT_PLATFORM_UNIX 1
#elif defined(_POSIX_VERSION)
  // Posix
  #define BRT_PLATFORM_POSIX 1
#elif __APPLE__
  // Apple platforms
  #define BRT_PLATFORM_APPLE 1
  #include <TargetConditionals.h>
  #if TARGET_IPHONE_SIMULATOR
    // iOS Simulator
    #define BRT_PLATFORM_IOS 1
    #define BRT_PLATFORM_IOS_SIMULATOR 1
  #elif TARGET_OS_IPHONE
    // iOS device
    #define BRT_PLATFORM_IOS 1
  #else
    #error "Unknown Apple platform"
  #endif
#else
  #error "Unknown platform!"
#endif
